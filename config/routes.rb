WebClientUi::Application.routes.draw do
  get "session/login"
  get "session/dashboard"
  get "session/users"
  get "session/billing"
  get "session/project_list"
  get "session/bundled_computes"
  get "session/load_balancers"
  get "session/accnt_owner"
  get "session/forget_password"
  get "session/sign_up"
  get "session/cloud_builder"
  get "session/volumes"

  root :to => 'session#login'
end
