class SessionController < ApplicationController

  def login
    render :partial => '/session/login'
  end

  def dashboard
  end

  def projects
  end

  def users
  end

  def billing
  end

  def project_list
  end

  def bundled_computes
  end

  def load_balancers
  end

  def accnt_owner
  end
  
  def cloud_builder
  end
  
  def volumes
  end

  def forget_password
    render :partial => '/session/forget_password'
  end

  def sign_up
    render :partial => '/session/sign_up'
  end

end
