// PLACE JAVASCRIPT FILES UNDER /vendor/assets/javascripts HERE
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require modernizr
//= require handlebars
//= require backbone-rails
//= require raphael
//= require g.raphael
//= require g.pie
//= require jquery.qtip

// PLACE JAVASCRIPT FILES UNDER /app/assets/javascripts HERE
//= require custom
//= require_tree ./views/general
//= require_tree ./views/dashboard
//= require_tree ./views/projects
//= require_tree ./views/projects/project_list
//= require_tree ./views/projects/bundled_computes
//= require_tree ./views/projects/load_balancers
//= require_tree ./views/users

//= require_self