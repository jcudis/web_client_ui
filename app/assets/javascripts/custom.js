	function sliderChange(val) {
		if (val == 1)
		{
			$('.flavor-description').html('<p><strong id="slider-status">Small</strong></p><dl><dt>CPU</dt><dd>core i7</dd></dl><dl><dt>RAM</dt><dd>1.5 GB</dd></dl><dl><dt>HD</dt><dd>1 TB</dd></dl>');
		}
		else if (val == 2)
		{
			$('.flavor-description').html('<p><strong id="slider-status">Medium</strong></p><dl><dt>CPU</dt><dd>core i7</dd></dl><dl><dt>RAM</dt><dd>2 GB</dd></dl><dl><dt>HD</dt><dd>1 TB</dd></dl>');
		}
		else if (val == 3)
		{
			$('.flavor-description').html('<p><strong id="slider-status">Large</strong></p><dl><dt>CPU</dt><dd>core i7</dd></dl><dl><dt>RAM</dt><dd>3 GB</dd></dl><dl><dt>HD</dt><dd>1 TB</dd></dl>');
		}
		else if (val == 4)
		{
			$('.flavor-description').html('<p><strong id="slider-status">XLarge</strong></p><dl><dt>CPU</dt><dd>core i7</dd></dl><dl><dt>RAM</dt><dd>4 GB</dd></dl><dl><dt>HD</dt><dd>1 TB</dd></dl>');
		}
		else 
		{
			$('.flavor-description').html('<p><strong id="slider-status">XSmall</strong></p><dl><dt>CPU</dt><dd>core i7</dd></dl><dl><dt>RAM</dt><dd>1 GB</dd></dl><dl><dt>HD</dt><dd>1 TB</dd></dl>');
		}

	}
	
$(document).ready(function(){
	//Initialize Heigts for main content and sidebar
	function initHeight() {
		var hHeight = $(".l-head").outerHeight();
		var wHeight = $(window).outerHeight();

		$(".l-main-content").css("height", wHeight - hHeight - 63);
		$(".l-sidebar").css("height", wHeight);
	}

  function createDashboardGraph() {
    var pieData1 = {
      title: "VM 1",
      info_1: "instance 1",
      info_2: "name 1",
      info_3: "memory 1",
      info_4: "external ip 1",
      info_5: "internal ip 1"
    };

    var pieData2 = {
      title: "VM 2",
      info_1: "instance 2",
      info_2: "name 2",
      info_3: "memory 2",
      info_4: "external ip 2",
      info_5: "internal ip 2"
    };

    var pieData3 = {
      title: "VM 3",
      info_1: "instance 3",
      info_2: "name 3",
      info_3: "memory 3",
      info_4: "external ip 3",
      info_5: "internal ip 3"
    };

    var pieData4 = {
      title: "VM 4",
      info_1: "instance 4",
      info_2: "name 4",
      info_3: "memory 4",
      info_4: "external ip 4",
      info_5: "internal ip 4"
    };

    var pieData5 = {
      title: "VM 5",
      info_1: "instance 5",
      info_2: "name 5",
      info_3: "memory 5",
      info_4: "external ip 5",
      info_5: "internal ip 5"
    };

    var pieData6 = {
      title: "VM 6",
      info_1: "instance 6",
      info_2: "name 6",
      info_3: "memory 6",
      info_4: "external ip 6",
      info_5: "internal ip 6"
    };

    var chartContainer = Raphael('holder');

    var pieChart = chartContainer.piechart(150, 150, 100,
      [15, 55, 20, 13, 20, 50],
      [pieData1, pieData2, pieData3, pieData4, pieData5, pieData6],
      {
        colors: ['green', 'red', 'red', 'green', 'yellow','orange'],
      },false
    );

    var labelContainer = chartContainer.circle(150, 150, 80);
    var mainLabel = chartContainer.text(150, 130, "Project 1").attr({ font: "20px sans-serif" });
    var subLabel = chartContainer.text(150, 150, "6 instances").attr({ font: "12px sans-serif" })
    labelContainer.attr({fill: "#fff", stroke: "#fff"});

    pieChart.each(function(sector, cover, i) {

      var content1 = $('<p>Instance ID: </p>');
      content1.append(this.sector.content.data.info_1);
      var content2 = $('<p>Name: </p>');
      content2.append(this.sector.content.data.info_2);
      var content3 = $('<p>Memory: </p>');
      content3.append(this.sector.content.data.info_3);
      var content4 = $('<p>External IP: </p>');
      content4.append(this.sector.content.data.info_4);
      var content5 = $('<p>Internal IP: </p>');
      content5.append(this.sector.content.data.info_5);

      this.sector.node.id = "slice_" + this.sector.id;
      $(this.sector.node).qtip({
          content: {
            title: this.sector.content.data.title,
            text: content1.html() + "<br />"
                  + content2.html() + "<br />"
                  + content3.html() + "<br />"
                  + content4.html() + "<br />"
                  + content5.html()
          },
          position: {
               my: 'bottom left',
               target: [this.sector.outer.x + 340, this.sector.outer.y + 100],
               viewport: $(window)
           },
           hide: {
               fixed: true
          },
           style: 'ui-tooltip-shadow'
      });
    });

    pieChart.hover(
      function() {
        this.sector.stop();
        this.sector.scale(1.1, 1.1, this.cx, this.cy);

        $(this.sector.node).qtip('show');
      }
    , function() {
        $(this.sector.node).qtip('hide');
        this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
      });


      //2nd pie
      var chartContainer2 = Raphael('holder2');
      var pieChart2 = chartContainer2.piechart(150, 150, 100,
        [15, 55, 20, 13],
        [pieData1, pieData2, pieData3, pieData4],
        {
          colors: ['green', 'red', 'red', 'yellow'],
        },false
      );

      var labelContainer2 = chartContainer2.circle(150, 150, 80);
      var mainLabel2 = chartContainer2.text(150, 130, "Project 2").attr({ font: "20px sans-serif" });
      var subLabel2 = chartContainer2.text(150, 150, "4 instances").attr({ font: "12px sans-serif" })
      labelContainer2.attr({fill: "#fff", stroke: "#fff"});

      pieChart2.each(function(sector, cover, i) {
        console.log(this);

        var content1 = $('<p>Instance ID: </p>');
        content1.append(this.sector.content.data.info_1);
        var content2 = $('<p>Name: </p>');
        content2.append(this.sector.content.data.info_2);
        var content3 = $('<p>Memory: </p>');
        content3.append(this.sector.content.data.info_3);
        var content4 = $('<p>External IP: </p>');
        content4.append(this.sector.content.data.info_4);
        var content5 = $('<p>Internal IP: </p>');
        content5.append(this.sector.content.data.info_5);

        this.sector.node.id = "slice_" + this.sector.id;
        $(this.sector.node).qtip({
            content: {
              title: this.sector.content.data.title,
              text: content1.html() + "<br />"
                    + content2.html() + "<br />"
                    + content3.html() + "<br />"
                    + content4.html() + "<br />"
                    + content5.html()
            },
            position: {
                 my: 'bottom left',
                 target: [this.sector.outer.x + 625, this.sector.outer.y + 100],
                 viewport: $(window)
             },
             hide: {
                 fixed: true
            },
             style: 'ui-tooltip-shadow'
        });
      });

      pieChart2.hover(
        function() {
          this.sector.stop();
          this.sector.scale(1.1, 1.1, this.cx, this.cy);

          $(this.sector.node).qtip('show');
        }
      , function() {
          $(this.sector.node).qtip('hide');
          this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
        });

         //3rd pie
          var chartContainer3 = Raphael('holder3');
          var pieChart3 = chartContainer3.piechart(150, 150, 100,
            [15, 55, 20],
            [pieData1, pieData2, pieData3],
            {
              colors: ['green', 'red', 'yellow'],
            },false
          );

          var labelContainer3 = chartContainer3.circle(150, 150, 80);
          var mainLabel3 = chartContainer3.text(150, 130, "Project 3").attr({ font: "20px sans-serif" });
          var subLabel3 = chartContainer3.text(150, 150, "4 instances").attr({ font: "12px sans-serif" })
          labelContainer3.attr({fill: "#fff", stroke: "#fff"});

          pieChart3.each(function(sector, cover, i) {
            console.log(this);

            var content1 = $('<p>Instance ID: </p>');
            content1.append(this.sector.content.data.info_1);
            var content2 = $('<p>Name: </p>');
            content2.append(this.sector.content.data.info_2);
            var content3 = $('<p>Memory: </p>');
            content3.append(this.sector.content.data.info_3);
            var content4 = $('<p>External IP: </p>');
            content4.append(this.sector.content.data.info_4);
            var content5 = $('<p>Internal IP: </p>');
            content5.append(this.sector.content.data.info_5);

            this.sector.node.id = "slice_" + this.sector.id;
            $(this.sector.node).qtip({
                content: {
                  title: this.sector.content.data.title,
                  text: content1.html() + "<br />"
                        + content2.html() + "<br />"
                        + content3.html() + "<br />"
                        + content4.html() + "<br />"
                        + content5.html()
                },
                position: {
                     my: 'bottom left',
                     target: [this.sector.outer.x + 340, this.sector.outer.y + 400],
                     viewport: $(window)
                 },
                 hide: {
                     fixed: true
                },
                 style: 'ui-tooltip-shadow'
            });
          });

          pieChart3.hover(
            function() {
              this.sector.stop();
              this.sector.scale(1.1, 1.1, this.cx, this.cy);

              $(this.sector.node).qtip('show');
            }
          , function() {
              $(this.sector.node).qtip('hide');
              this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
            });
  }

	$('#myModal').modal({
    backdrop: true,
    keyboard: false,
    show: false
  });

  $('#modalEditUser').modal({
    backdrop: true,
    keyboard: false,
    show: false
  });

  $('.droppable').click(function(){
 		$(this).parent().parent().find('.project-body').toggleClass('is-hidden');
 		$(this).parent().first('li').toggleClass('row-selected');
 	});

  $('.tbody').hover(function() {
    $('.thead').addClass('header-greyed');
  }, function() {
    $('.thead').removeClass('header-greyed');
  });

  $('.tbody').click(function() {
    $('#modalEditUser').modal({
      backdrop: true,
      keyboard: false,
      show: true
    });
  });

  //Temporary implementation of selecting the active navigation
  function setActiveNavigation() {
    var path = window.location.pathname;
    if(path == '/session/dashboard') {
      $('a#dashboard').addClass('active');
      createDashboardGraph();
    }
    if(path == '/session/projects') {
      $('a#projects').addClass('active');
      $('ul.subnav').removeClass('is-hidden');
    }
    if(path == '/session/project_list') {
      $('ul.subnav').removeClass('is-hidden');
      $('a#projects').addClass('active');
      $('a#project_list').addClass('active');
    }
    if(path == '/session/bundled_computes') {
      $('ul.subnav').removeClass('is-hidden');
      $('a#projects').addClass('active');
      $('a#bundled_computes').addClass('active');

    }
    if(path == '/session/load_balancers') {
      $('ul.subnav').removeClass('is-hidden');
      $('a#projects').addClass('active');
      $('a#load_balancers').addClass('active');
    }
    if(path == '/session/users') {
      $('a#users').addClass('active');
    }
    if(path == '/session/billing') {
      $('a#billing').addClass('active');
    }
  }

	initHeight();
	setActiveNavigation();

	$(window).resize(function(){
		initHeight();
	});

	$('.tabs').tab('show');

	$('*[rel=tooltip]').tooltip();

	
});
